import React from 'react'
import {Link} from 'react-router-dom'
import logo3 from './logo3.png'

export default function Navbar() {
    return (
        <nav className='navbar navbar-expand-lg navbar-dark bg-dark'>
            <div className="container-fluid">
                <a className="navbar-brand" href="/">Tienda Familiar v1.0</a>

                <img src={logo3} alt="svg" width="120"/>
                
                <ul className="navbar-nav">
                    <li className="nav-item">
                        <Link className="nav-link active" to="/">Main</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link active" to="/crear">Crear Articulo</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link active" to="/listar">Listar</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link active" to="/about">About</Link>
                    </li>
                </ul>
            </div>
        </nav>
    )
}
