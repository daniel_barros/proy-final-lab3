import { useEffect, useState } from "react";
import Articulo from './Articulo'

const LArticulo = () => {

    const [items, setItems] = useState([]);

    useEffect(() => {
        fetch('http://localhost:4000/items').then((response) => {
            response.json().then((data) => {
                console.log(data.mensaje)
                setItems(data.data)
            })
        })
    }, [])

    const onDelete = async(id) => {
        const response = await fetch('http://localhost:4000/items/'+ id,{
            method: 'DELETE'
        })
        
        if (response.ok){
            console.log('Item '+{id}+' eliminado')
            const itemsTemp = items.filter((item) => item.id !== id)
            setItems(itemsTemp)
        }else{
            console.log('No se pudo eliminar el item: '+ id )
        }
    }


    return (
        <>
            <h2 className='text-center'>Listado de Articulos</h2>
            <table className="table table-hover">
                <thead>
                    <tr>
                        <th scope="col" className='text-center'></th>
                        <th scope="col" className='text-center'>Codigo</th>                        
                        <th scope="col" className='text-center'>Nombre</th>
                        <th scope="col" className='text-center'>Marca</th>
                        <th scope="col" className='text-center'>Categoria</th>
                        <th scope="col" className='text-center'>Distribuidor</th>
                        <th scope="col" className='text-center'>Cod. Barras</th>
                        <th scope="col" className='text-center'>Stock</th>
                        <th scope="col" className='text-center'>Precio</th>
                        <th scope="col" className='text-center'>Descripcion</th>
                    </tr>
                </thead>
                <tbody>
                    {items.map((articulo) => <Articulo key={articulo.id} item={articulo} onDelete={onDelete} />)}
                </tbody>
            </table>
        </>
    );
}

export default LArticulo;