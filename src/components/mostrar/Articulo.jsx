
const Articulo = (props) => {
    return (
        <tr className="table-primary  text-center">
            <td>
                <button className="btn btn-outline-light btn-sm mr-1" onClick={()=> props.onDelete(props.item.id)}>
                    X
                </button>
                <span> </span>
                <button className="btn btn-outline-light btn-sm text-light mr-1" onClick={() => props.onModificar(props.item.id)} >
                    M
                </button>
            </td>
            <td>{props.item.codigo}</td>            
            <td>{props.item.nombre}</td>
            <td>{props.item.marca}</td>
            <td>{props.item.categoria}</td>
            <td>{props.item.distribuidor}</td>
            <td>{props.item.barcode}</td>
            <td>{props.item.stock}</td>
            <td>{props.item.precio}</td>
            <td>{props.item.descripcion}</td>
        </tr>
    );
}

export default Articulo;