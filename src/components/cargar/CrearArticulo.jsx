import { useState } from "react";

const Tareas = () => {

    const [nuevoCodigo, setnuevoCodigo] = useState('');
    const [nuevoNombre, setnuevoNombre] = useState('');
    const [nuevaMarca, setnuevaMarca] = useState('');
    const [nuevaCategoria, setnuevaCategoria] = useState('')
    const [nuevoDistribuidor, setnuevoDistribuidor] = useState('')
    const [nuevoBarcode, setnuevoBarcode] = useState('')
    const [nuevoStock, setNuevoStock] = useState('')
    const [nuevoPrecio, setnuevoPrecio] = useState('')
    const [nuevaDescripcion, setnuevaDescripcion] = useState('')

    const [articulos, setArticulos] = useState([])

    //Funcion para limpiar los inputs despues de cargarlos
    function resetValues () {
        setnuevoCodigo('');
        setnuevoNombre('');
        setnuevaMarca('');
        setnuevaCategoria('');
        setnuevoDistribuidor('');
        setnuevoBarcode('');
        setNuevoStock('');
        setnuevoPrecio('');
        setnuevaDescripcion('');
    }
    
    // Post a API para crear tarea
    const onNuevoArticulo = async () => {
        // Post a API para crear tarea
        const response = await fetch('http://localhost:4000/items', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                codigo: nuevoCodigo,
                nombre: nuevoNombre,
                marca: nuevaMarca,
                categoria: nuevaCategoria,
                distribuidor: nuevoDistribuidor,
                barcode: nuevoBarcode,
                stock: nuevoStock,
                enabled: 1,
                precio: nuevoPrecio,
                descripcion: nuevaDescripcion
            })
        })
        if (response.ok) {
            const data = await response.json()
            setArticulos([...articulos, data.data])
            alert('Articulo agragdo con éxito!')
        } else {
            alert('Error al agregar una tarea')
        }

        resetValues()

    }

    return (
        <>
            <h2 className='text-center'>Crear articulo</h2>

            <div className='centrar-caja'>
                <br />
                <input className='mt-2 largo-inputs' value={nuevoCodigo} onChange={(e) => setnuevoCodigo(e.target.value)} placeholder='Codigo Interno'/><br />
                <input className='mt-2 largo-inputs' value={nuevoNombre} onChange={(e) => setnuevoNombre(e.target.value)} placeholder='Nombre del Producto'/><br />
                <input className='mt-2 largo-inputs' value={nuevaMarca} onChange={(e) => setnuevaMarca(e.target.value)} placeholder='Marca del producto'/> <br />
                <input className='mt-2 largo-inputs' value={nuevaCategoria} onChange={(e) => setnuevaCategoria(e.target.value)} placeholder='Categoria del producto' /><br />
                <input className='mt-2 largo-inputs' value={nuevoDistribuidor} onChange={(e) => setnuevoDistribuidor(e.target.value)} placeholder='Nombre del distibuidor' /><br />
                <input className='mt-2 largo-inputs' value={nuevoBarcode} onChange={(e) => setnuevoBarcode(e.target.value)} placeholder='Codigo de Barras'/><br />
                <input className='mt-2 largo-inputs' value={nuevoStock} onChange={(e) => setNuevoStock(e.target.value)} placeholder='Stock Inicial' /><br />
                <input className='mt-2 largo-inputs' value={nuevoPrecio} onChange={(e) => setnuevoPrecio(e.target.value)} placeholder='Precio Unitario' /><br />
                <input className='mt-2 largo-inputs' value={nuevaDescripcion} onChange={(e) => setnuevaDescripcion(e.target.value)} placeholder='Descripcion' /><br /><br />

                <button className="btn btn-primary largo-inputs" onClick={onNuevoArticulo}>Agregar Articulo</button><br />

            </div>



        </>
    );
}

export default Tareas;
