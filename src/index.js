import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Navbar from './components/navbar/Navbar';
import ListarArticulos from './components/mostrar/ListarArticulos'
import Home from './components/Home'
import Main from './components/Main'
import CrearArticulo from './components/cargar/CrearArticulo'


ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Navbar></Navbar>
      <Switch> 

        <Route component={Home} exact path='/home'></Route>
        <Route component={CrearArticulo} exact path='/crear'></Route>
        <Route component={ListarArticulos} exact path='/listar'></Route>
        <Route component={Main} exact path='/'></Route>

      </Switch>
    </BrowserRouter>



    {/* <App /> */}
  </React.StrictMode>,
  document.getElementById('root')
);


